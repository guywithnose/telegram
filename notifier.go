package telegram

import (
	"fmt"
	"net/http"
	"net/url"
	"strconv"
)

// Notifier sends a notification to telegram
type Notifier struct {
	apiToken string
	chatID   int
	baseURL  string
}

// NewNotifier creates a new Notifier
func NewNotifier(apiToken string, chatID int) *Notifier {
	return &Notifier{apiToken: apiToken, chatID: chatID, baseURL: "https://api.telegram.org"}
}

// Send sends a notification to telegram
func (n Notifier) Send(text string) error {
	queryParams := url.Values{
		"chat_id": []string{strconv.Itoa(n.chatID)},
		"text":    []string{text},
	}

	resp, err := http.Get(fmt.Sprintf("%s/bot%s/sendMessage?%s", n.baseURL, n.apiToken, queryParams.Encode()))
	if resp.StatusCode != 200 {
		return fmt.Errorf("Unable to notify telegram. Status code: %d", resp.StatusCode)
	}

	return err
}
