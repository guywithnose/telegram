package telegram

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
)

// Listener sends a notification to telegram
type Listener struct {
	apiToken string
	baseURL  string
	offset   int
}

// Message contains info about a telegram message
type Message struct {
	ChatID int
	Text   string
	From   string
}

type updateResponse struct {
	Result []update `json:"result"`
	Ok     bool     `json:"ok"`
}

type update struct {
	ID      int     `json:"update_id"`
	Message message `json:"message"`
}

type message struct {
	From user   `json:"from"`
	Chat chat   `json:"chat"`
	Text string `json:"text"`
}

type user struct {
	Username string `json:"username"`
}

type chat struct {
	ID int `json:"id"`
}

// NewListener creates a new Listener
func NewListener(apiToken string) *Listener {
	return &Listener{apiToken: apiToken, baseURL: "https://api.telegram.org"}
}

// Listen long polls telegram for new messages
func (l Listener) Listen() chan Message {
	listener := make(chan Message)
	go func() {
		for {
			queryParams := url.Values{
				"timeout": []string{"300"},
			}

			if l.offset > 0 {
				queryParams["offset"] = []string{strconv.Itoa(l.offset)}
			}

			resp, err := http.Get(fmt.Sprintf("%s/bot%s/getUpdates?%s", l.baseURL, l.apiToken, queryParams.Encode()))
			if err == nil {
				content, err := ioutil.ReadAll(resp.Body)
				if err == nil {
					response := new(updateResponse)
					err = json.Unmarshal(content, response)
					if err == nil {
						for _, update := range response.Result {
							if update.ID >= l.offset {
								l.offset = update.ID + 1
							}

							m := Message{
								ChatID: update.Message.Chat.ID,
								Text:   update.Message.Text,
								From:   update.Message.From.Username,
							}

							listener <- m
						}
					}
				}
			}
		}
	}()

	return listener
}
