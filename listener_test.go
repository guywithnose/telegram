package telegram

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestListener(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		response1 := `{"ok":true,"result":[{"update_id":43653413, "message":{"from":{"username":"guywithnose"}` +
			`,"chat":{"id":344195656},"date":1543212935,"text":"message1"}}]}`
		response2 := `{"ok":true,"result":[{"update_id":43653414, "message":{"from":{"username":"guywithnose"}` +
			`,"chat":{"id":344195656},"date":1543212935,"text":"message2"}}]}`

		if r.FormValue("offset") != "" {
			_, _ = fmt.Fprintln(w, response2)
		} else {
			_, _ = fmt.Fprintln(w, response1)
		}
	}))
	defer ts.Close()
	tele := NewListener("token")
	tele.baseURL = ts.URL
	listener := tele.Listen()
	message := <-listener
	assert.Equal(t, "message1", message.Text)
	message = <-listener
	assert.Equal(t, "message2", message.Text)
}
