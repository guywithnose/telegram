package telegram

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSend(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, "123", r.FormValue("chat_id"))
		assert.Equal(t, "test message", r.FormValue("text"))
		assert.Equal(t, "/bottoken/sendMessage", r.URL.Path)
		_, _ = fmt.Fprintln(w, "Hello, client")
	}))
	defer ts.Close()
	tele := NewNotifier("token", 123)
	tele.baseURL = ts.URL
	assert.Nil(t, tele.Send("test message"))
}

func TestSendError(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusInternalServerError)
	}))
	defer ts.Close()
	tele := NewNotifier("token", 123)
	tele.baseURL = ts.URL
	assert.EqualError(t, tele.Send("test message"), "Unable to notify telegram. Status code: 500")
}
